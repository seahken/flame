import { Firecamp } from "./../packages/Firecamp/index.js";

const options = {
  soundSensitivity: 0.5,
}

const fire = new Firecamp('#firecamp', options)

document.querySelector('#init').onmousedown = function() {
  fire.init()
}

document.querySelector('#activate').onclick = function () {
  fire.activateVoice()
}

document.querySelector('#pause').onclick = function() {
  fire.pause()
}

document.querySelector('#destroy').onmousedown = function() {
  fire.destroy()
}
