'use strict';
import fireImage from "./assets/flame.svg"
import {
  SoundMeter
} from "./SoundMeter.js"

class Firecamp {
  constructor(element, options) {
    this.element = element;
    this.options = options;
    this.soundSensitivity = 1 / options.soundSensitivity / 10;
    this.div = document.querySelector(this.element);
  }

  init() {
    if (!document.querySelector('#fire')) {
      this.div.innerHTML += '<div id="fire"></div>';
      let fireDiv = document.querySelector('#fire');
      fireDiv.innerHTML += fireImage
      this.div.classList.remove('pause');
    }
  }

  activateVoice() {
    let div = document.querySelector('#fire');
    if (!document.querySelector('#fire-instruction')) {
      div.innerHTML += '<p id="fire-instruction">Trying blowing into your microphone</p>';
    }
    div.classList.remove('pause');
    try {
      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      window.audioContext = new AudioContext();
    } catch (e) {
      alert('Web Audio API not supported.');
    }
    navigator.mediaDevices.getUserMedia({
      audio: true
    })
    .then((stream)=> {
      window.stream = stream;
      const soundMeter = window.soundMeter = new SoundMeter(window.audioContext);
      soundMeter.connectToSource(stream, (e) => {
        if (e) {
          alert(e);
          return;
        }
        setInterval(() => {
          this.animateWithSound(soundMeter.instant.toFixed(2));
        }, 200);
      });
    })
    .catch(function(err) {
      console.log(err);
    });
  }

  animateWithSound(soundLevel = 0) {
    let div = document.querySelector('#fire');
    if(soundLevel < this.soundSensitivity) {
      div.classList.remove('highly-agitated');
      div.classList.add('agitated');
    } else {
      div.classList.add('highly-agitated');
    }
  }

  pause() {
    soundMeter.stop();
    let div = document.querySelector('#fire');
    div.classList.add('pause');
  }

  destroy() {
    let div = document.querySelector('#fire');
    div.remove();
  }
}

export {
  Firecamp
}
