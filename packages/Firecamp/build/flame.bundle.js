/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./Firecamp.js":
/*!*********************!*\
  !*** ./Firecamp.js ***!
  \*********************/
/*! exports provided: Firecamp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Firecamp", function() { return Firecamp; });
/* harmony import */ var _assets_flame_svg__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets/flame.svg */ "./assets/flame.svg");
/* harmony import */ var _assets_flame_svg__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_flame_svg__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SoundMeter_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SoundMeter.js */ "./SoundMeter.js");





class Firecamp {
  constructor(element, options) {
    this.element = element;
    this.options = options;
    this.soundSensitivity = 1 / options.soundSensitivity / 10;
    this.div = document.querySelector(this.element);
  }

  init() {
    if (!document.querySelector('#fire')) {
      this.div.innerHTML += '<div id="fire"></div>';
      let fireDiv = document.querySelector('#fire');
      fireDiv.innerHTML += _assets_flame_svg__WEBPACK_IMPORTED_MODULE_0___default.a;
      this.div.classList.remove('pause');
    }
  }

  activateVoice() {
    let div = document.querySelector('#fire');

    if (!document.querySelector('#fire-instruction')) {
      div.innerHTML += '<p id="fire-instruction">Trying blowing into your microphone</p>';
    }

    div.classList.remove('pause');

    try {
      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      window.audioContext = new AudioContext();
    } catch (e) {
      alert('Web Audio API not supported.');
    }

    navigator.mediaDevices.getUserMedia({
      audio: true
    }).then(stream => {
      window.stream = stream;
      const soundMeter = window.soundMeter = new _SoundMeter_js__WEBPACK_IMPORTED_MODULE_1__["SoundMeter"](window.audioContext);
      soundMeter.connectToSource(stream, e => {
        if (e) {
          alert(e);
          return;
        }

        setInterval(() => {
          this.animateWithSound(soundMeter.instant.toFixed(2));
        }, 200);
      });
    }).catch(function (err) {
      console.log(err);
    });
  }

  animateWithSound(soundLevel = 0) {
    let div = document.querySelector('#fire');

    if (soundLevel < this.soundSensitivity) {
      div.classList.remove('highly-agitated');
      div.classList.add('agitated');
    } else {
      div.classList.add('highly-agitated');
    }
  }

  pause() {
    soundMeter.stop();
    let div = document.querySelector('#fire');
    div.classList.add('pause');
  }

  destroy() {
    let div = document.querySelector('#fire');
    div.remove();
  }

}



/***/ }),

/***/ "./SoundMeter.js":
/*!***********************!*\
  !*** ./SoundMeter.js ***!
  \***********************/
/*! exports provided: SoundMeter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SoundMeter", function() { return SoundMeter; });
/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
 // Meter class that generates a number correlated to audio volume.
// The meter class itself displays nothing, but it makes the
// instantaneous and time-decaying volumes available for inspection.
// It also reports on the fraction of samples that were at or near
// the top of the measurement range.

function SoundMeter(context) {
  this.context = context;
  this.instant = 0.0;
  this.slow = 0.0;
  this.clip = 0.0;
  this.script = context.createScriptProcessor(2048, 1, 1);
  const that = this;

  this.script.onaudioprocess = function (event) {
    const input = event.inputBuffer.getChannelData(0);
    let i;
    let sum = 0.0;
    let clipcount = 0;

    for (i = 0; i < input.length; ++i) {
      sum += input[i] * input[i];

      if (Math.abs(input[i]) > 0.99) {
        clipcount += 1;
      }
    }

    that.instant = Math.sqrt(sum / input.length);
    that.slow = 0.95 * that.slow + 0.05 * that.instant;
    that.clip = clipcount / input.length;
  };
}

SoundMeter.prototype.connectToSource = function (stream, callback) {
  try {
    this.mic = this.context.createMediaStreamSource(stream);
    this.mic.connect(this.script); // necessary to make sample run, but should not be.

    this.script.connect(this.context.destination);

    if (typeof callback !== 'undefined') {
      callback(null);
    }
  } catch (e) {
    console.error(e);

    if (typeof callback !== 'undefined') {
      callback(e);
    }
  }
};

SoundMeter.prototype.stop = function () {
  this.mic.disconnect();
  this.script.disconnect();
};



/***/ }),

/***/ "./assets/flame.svg":
/*!**************************!*\
  !*** ./assets/flame.svg ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"-0.4 -0.5 214 342\"><path d=\"M117.7 74.3c-.3 23.4-11.2 45.3-19.3 66.8-9 24-17.6 48-27.6 71.5A655.8 655.8 0 0 1 0 336c-.2.2 0 .4.1.5 10.8 6.2 25.6 4.2 37.5 3.4 18.2-1.4 36.3-1.2 54.6-1.2h64.4c18.5.2 36.9-.6 54.6-6.2.4-.1 1-.4.8-.8-4.4-20.7-22-36.2-33-53.5-13.8-21.8-19.4-46.3-23.8-71.4a742.2 742.2 0 0 0-16.5-76.6c-3-10.2-6-20.3-8.6-30.6a90.8 90.8 0 0 0-11.3-28c-.4-.5-2.3.2-1.8.9 6 8.7 8.8 19 11.5 29.2 2.8 10.3 5.8 20.5 8.8 30.8 7.2 25.3 11.7 50.9 16.3 76.7 4.1 23.5 9.7 46.1 21.9 66.9 10.9 18.6 30 34.3 34.6 56.1l.8-.8c-15 4.7-30.7 6-46.4 6.1-16 .2-31.8 0-47.7 0-27.8 0-55.4-.4-83.1 1.5-5.8.3-11.7.5-17.5.2a51.4 51.4 0 0 1-11.2-2c-2.9-1-2.9-1.6-1.1-4a757.2 757.2 0 0 0 25.4-38.4 615 615 0 0 0 43.2-82.2c9.6-22.4 18-45.3 26.4-68.1 8.5-22.7 20.3-45.8 20.6-70.5 0-.9-1.9-.5-2 .2z\" id=\"fire-1\"></path><path d=\"M107.5 20.7c-.5 5.6 1.3 11 2 16.4l2-.6c-5.5-3.6-11-11.2-3.7-16 .9-.5-1-.3-1.4 0-7.7 4.8-2.8 12.7 3.2 16.6.3.3 2 0 1.9-.5-.8-5.5-2.6-10.8-2-16.4 0-.3-2 0-2 .5z\" id=\"smoke-1\"></path><path d=\"M139.8 75.4c8.4 47.6-24.2 85.6-51 120.8A387.3 387.3 0 0 0 69.6 223c-7.2 11.1-13.1 23-19.5 34.5a788.9 788.9 0 0 1-49.1 78.6c-.2.2 0 .4.1.5 10.8 6.2 25.6 4.2 37.5 3.4 18.2-1.4 36.3-1.2 54.6-1.2h64.4c18.5.2 36.9-.6 54.6-6.2.4-.1 1-.4.8-.8-5.6-26-30.6-41.8-39.8-66-8.9-23.4-2.8-47-.2-70.8 4.9-43.7 1.4-90.7-32.8-122.3-.6-.5-2.4.2-1.8.8 35.4 32.7 37.4 81.2 32.2 126.2-1.3 10.9-3.6 21.8-4.2 32.8-.7 11.4 1 23 4.8 33.7 8.8 24.6 34.3 40 40 66.1l.7-.8c-15 4.7-30.7 6-46.4 6.1-16 .2-31.8 0-47.7 0-27.8 0-55.4-.4-83.1 1.5-5.8.3-11.7.5-17.5.2a51.4 51.4 0 0 1-11.2-2c-3-1-2.8-1.6-1-4.1a757 757 0 0 0 26-39.4c15-24.3 27-50.3 42.6-74.1 29-44.3 78.3-86.3 68-144.8 0-.8-2-.3-1.9.5z\" id=\"fire-right-1\"></path><path d=\"M164.5 75.4c11 25.2-5.5 48.1-22.5 65.7-18.3 18.9-38.2 35.8-54.8 56.3-16.8 20.8-28.6 44.2-41.5 67.3A757 757 0 0 1 1 335.1c-.2.2 0 .4.1.5 10.8 6.2 25.6 4.2 37.5 3.4 18.2-1.4 36.3-1.2 54.6-1.2h64.4c18.5.2 36.9-.6 54.6-6.2.4-.1 1-.4.8-.8-2.9-13.5-11.9-25-20-35.7-4.2-5.4-8.8-10.3-12.6-16a72.2 72.2 0 0 1-9.3-21c-7.1-25.1 3.6-49.5 14.7-71.7 10-20.2 23.5-42.6 19.4-66-3.8-22-22.6-38.5-41.6-48-.6-.2-2.4.7-1.5 1 20.5 10.3 39 27.8 41.8 51.6 1.5 12.8-3.2 25.5-8.2 37-4.7 11-10.5 21.6-15.7 32.3-10 20.9-16.9 43-10.4 66 7.6 27.1 35.5 43.2 41.5 71l.8-.8c-15 4.7-30.7 6-46.4 6.1-16 .2-31.8 0-47.7 0-27.8 0-55.4-.4-83.1 1.5-5.8.3-11.7.5-17.5.2a51.4 51.4 0 0 1-11.2-2c-3-1-2.8-1.6-1-4.1a757 757 0 0 0 26-39.4c16.3-26.4 29.1-55 47-80.5 18-25.8 40.7-47 63.3-68.6 18.4-17.5 36.5-42.7 25-68.8-.3-.8-2.2-.2-2 .5z\" id=\"fire-right-2\"></path><path d=\"M85.1 74.3C59.5 113 60.4 158.9 59.2 203.4c-.3 11.4-.4 23-2 34.2-1.8 12.3-9.2 24-15.2 34.7A721.6 721.6 0 0 1 1 336c0 .2 0 .4.2.5 10.8 6.2 25.6 4.2 37.5 3.4 18.2-1.4 36.3-1.2 54.6-1.2h64.4c18.5.2 36.9-.6 54.6-6.2.4-.1 1-.4.8-.8-4.6-22-23.9-38.2-35-56.9a179.7 179.7 0 0 1-15.8-36.5c-4-11.9-9-23.3-14.7-34.4-10.7-21-23.7-40.8-35.7-61.2-6.3-10.9-13.8-22.3-17.3-34.5C91.1 96.3 92 83.6 87 72c-.4-.7-2.3-.2-2 .5 4.9 11 4.6 23.1 7.2 34.7 2.6 11.3 9.7 22 15.4 32 12.5 21.9 26.5 43 38 65.3a250.4 250.4 0 0 1 14.2 32.8c4 11.6 8 23 14 33.9 5.8 10.7 13.2 20.1 20.6 29.8a83 83 0 0 1 16.8 31.3l.8-.8c-15 4.7-30.7 6-46.4 6.1-16 .2-31.8 0-47.7 0-27.8 0-55.4-.4-83.1 1.5-5.8.3-11.7.5-17.5.2a51.4 51.4 0 0 1-11.2-2c-3-1-2.8-1.6-1-4.1a757 757 0 0 0 26-39.4 391.5 391.5 0 0 0 23-41c6-13 6.2-26.9 6.8-41 1.9-47.2-1.3-96.2 26-137.4.6-.8-1.4-.7-1.8-.1z\" id=\"fire-left-1\"></path><path d=\"M55.3 73.7a82.8 82.8 0 0 0-30.9 83.6c2.4 11.7 7.8 22.6 12.4 33.5 9.6 22.4 20 48.4 10 72.4-2.5 5.7-6.5 10.3-9.6 15.7A713.3 713.3 0 0 1 0 336c-.2.2 0 .4.1.5 10.8 6.2 25.6 4.2 37.5 3.4 18.2-1.4 36.3-1.2 54.6-1.2h64.4c18.5.2 36.9-.6 54.6-6.2.4-.1 1-.4.8-.8-2.6-12.3-10.5-23-18-32.9a211.4 211.4 0 0 1-23.3-36c-6.2-13-9.6-26.8-15.8-39.6-5.7-12-13.4-22.8-22.2-32.7-14.7-16.5-32.3-30-47.8-45.8-14.8-15-27.6-32.7-29-54.3-.5-6.2.3-12.5 1.6-18.6.2-.8-1.8-.5-2 .2A69.9 69.9 0 0 0 69 129.3c12.8 17.6 30.3 31.3 46 46 19.4 18 35.6 38.5 44.4 63.8 4.6 13.2 9.6 26 16.8 38 7 11.8 16.2 22 23.8 33.2 4.5 6.8 8.5 14 10.2 22l.8-.8c-15 4.7-30.7 6-46.4 6.1-15.9.2-31.8 0-47.7 0-27.8 0-55.4-.4-83.1 1.5-5.8.3-11.6.5-17.5.2a51.4 51.4 0 0 1-11.2-2c-3-1-2.8-1.6-1-4.1 9-12.9 17.7-26 26-39.4 4-6.4 7.4-13.5 11.9-19.5a55 55 0 0 0 8.7-16.8 67.4 67.4 0 0 0-.7-35.9c-6.4-26-23-49-25.2-76a81.4 81.4 0 0 1 31.7-71.2c1-.8-.6-1.2-1.2-.7z\" id=\"fire-left-2\"></path><path d=\"M122 .7c-.4 5.6 1.6 11 2.6 16.4l1.8-.6c-5.5-3.4-12-11.1-4.2-16 .9-.5-1-.4-1.5-.1-8 5-2.2 13 4 16.8.3.2 1.9 0 1.8-.6-1-5.5-3-10.8-2.6-16.4 0-.4-2-.1-2 .5z\" id=\"smoke-2\"></path><path d=\"M85.4 42.2c-.5 5.6 1.3 10.9 2 16.3l2-.6c-5.5-3.5-11-11.1-3.6-15.9.8-.5-1.1-.3-1.5 0-7.7 4.8-2.8 12.7 3.2 16.6.4.3 2 0 1.9-.6-.8-5.4-2.5-10.7-2-16.3 0-.3-2-.1-2 .5z\" id=\"smoke-3\"></path></svg>"

/***/ }),

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! exports provided: Firecamp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Firecamp_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Firecamp.js */ "./Firecamp.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Firecamp", function() { return _Firecamp_js__WEBPACK_IMPORTED_MODULE_0__["Firecamp"]; });

/* harmony import */ var _styles_main_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles/main.scss */ "./styles/main.scss");
/* harmony import */ var _styles_main_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_main_scss__WEBPACK_IMPORTED_MODULE_1__);




/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./styles/main.scss":
/*!*********************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./styles/main.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#fire {\n  width: 20rem;\n  height: 20rem;\n  background-size: contain;\n  background-repeat: no-repeat; }\n  #fire #fire-left-1,\n  #fire #fire-left-2,\n  #fire #fire-right-1,\n  #fire #fire-right-2,\n  #fire #smoke-1,\n  #fire #smoke-2,\n  #fire #smoke-3 {\n    opacity: 0; }\n  #fire #fire-1 {\n    animation: resting 2s infinite; }\n  #fire #fire-right-1 {\n    animation: resting-one 2s infinite; }\n  #fire.agitated #fire-1,\n  #fire.agitated #smoke-1 {\n    animation: fire-one 4s infinite; }\n  #fire.agitated #fire-right-1,\n  #fire.agitated #smoke-2 {\n    animation: fire-right-one 4s infinite; }\n  #fire.agitated #fire-right-2 {\n    animation: fire-right-two 4s infinite; }\n  #fire.agitated #fire-left-1,\n  #fire.agitated #smoke-3 {\n    animation: fire-left-one 4s infinite; }\n  #fire.agitated #fire-left-2 {\n    animation: fire-left-two 4s infinite; }\n  #fire.highly-agitated path {\n    animation-duration: 0.5s !important; }\n  #fire.pause path {\n    animation: none !important; }\n\n@keyframes resting {\n  0% {\n    opacity: 1; }\n  50% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n\n@keyframes resting-one {\n  0% {\n    opacity: 0; }\n  50% {\n    opacity: 1; }\n  100% {\n    opacity: 0; } }\n\n@keyframes fire-one {\n  0% {\n    opacity: 1; }\n  12.5% {\n    opacity: 0; }\n  25% {\n    opacity: 0; }\n  37.5% {\n    opacity: 0; }\n  50% {\n    opacity: 1; }\n  62.5% {\n    opacity: 0; }\n  75% {\n    opacity: 0; }\n  87.5% {\n    opacity: 0; }\n  100% {\n    opacity: 0; } }\n\n@keyframes fire-right-one {\n  0% {\n    opacity: 0; }\n  12.5% {\n    opacity: 1; }\n  25% {\n    opacity: 0; }\n  37.5% {\n    opacity: 1; }\n  50% {\n    opacity: 0; }\n  62.5% {\n    opacity: 0; }\n  75% {\n    opacity: 0; }\n  87.5% {\n    opacity: 0; }\n  100% {\n    opacity: 0; } }\n\n@keyframes fire-right-two {\n  0% {\n    opacity: 0; }\n  12.5% {\n    opacity: 0; }\n  25% {\n    opacity: 1; }\n  37.5% {\n    opacity: 0; }\n  50% {\n    opacity: 0; }\n  62.5% {\n    opacity: 0; }\n  75% {\n    opacity: 0; }\n  87.5% {\n    opacity: 0; }\n  100% {\n    opacity: 0; } }\n\n@keyframes fire-left-one {\n  0% {\n    opacity: 0; }\n  12.5% {\n    opacity: 0; }\n  25% {\n    opacity: 0; }\n  37.5% {\n    opacity: 0; }\n  50% {\n    opacity: 0; }\n  62.5% {\n    opacity: 1; }\n  75% {\n    opacity: 0; }\n  87.5% {\n    opacity: 1; }\n  100% {\n    opacity: 0; } }\n\n@keyframes fire-left-two {\n  0% {\n    opacity: 0; }\n  12.5% {\n    opacity: 0; }\n  25% {\n    opacity: 0; }\n  37.5% {\n    opacity: 0; }\n  50% {\n    opacity: 0; }\n  62.5% {\n    opacity: 0; }\n  75% {\n    opacity: 1; }\n  87.5% {\n    opacity: 0; }\n  100% {\n    opacity: 0; } }\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/*!*************************************************!*\
  !*** ./node_modules/css-loader/lib/css-base.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./styles/main.scss":
/*!**************************!*\
  !*** ./styles/main.scss ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader!../node_modules/sass-loader/lib/loader.js!./main.scss */ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./styles/main.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

/******/ });
//# sourceMappingURL=flame.bundle.js.map