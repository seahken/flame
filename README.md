## Solution to Technical Test
Due to time contstraints, I've only created an animated flame-like triangle that reacts to the microphone input. The Flame is added using a Javascript library onto the DOM.

The only options available for the flame is the voice sensitivity. When using the firecamp
```
const fire = new Firecamp('#firecamp', {
  soundSensitivity: 0.5,
})
```
where soundSensitivity accepts the range between 0 and 1.

## Setup
To get setup, clone repository. run `yarn` or `npm install` inside the root directory.

To build JS and Styles, run `yarn watch` or `npm run watch`.

# Creative Developer
## Technical test


### The Brief

Using the below animated GIF as a reference / inspiration, create a ES6+ Javascript library able to dynamically generate an animated fire camp in the broswer viewport.

![](firecamp.gif)


### Rules

- Must be supported in Chrome browser.
- The library must be built as an ES6 class.
- The rendering of the fire should be done using SVG elements.
- We should have the ability to pass options. Feel free to include any relevant options that you think are necessary to be able to change the fire properties.


### Technical challenge

Make the firecamp react/respond to sound input from microphone OR camera input from the browser. Be creative, we want to see what you can come up with!


### Deliverables

- Link to a Github repository containing the ES6+ library
- Readme.md file containing:
	- Minimal documentation
	- Demo link to be able to quickly see the result
	- Simple breakdown of your approach for the technical challenge


### Sample code:

```html
<div id="firecamp" style="width:600px;height:450px;"></div>
```

```javascript
// import es6 class
import Firecamp from `firecamp`

// instantiate with targeting parent DOM element and options
const fire = new Firecamp('#firecamp', options)

// initiate and display the fire in the browser
fire.init()

// animate the fire
fire.animate()

// pause the fire animation
fire.pause()

// destroy and reset
fire.destroy()
```
